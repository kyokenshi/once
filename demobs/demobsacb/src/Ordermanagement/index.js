import React, { useState, useEffect } from "react";

import { Appp } from "../Khachhang/style";
import {
  Ordermanagement,
  OrderSearch,
  StyleCard,
  StylePagination,
} from "./style";

import axios from "axios";

import { Button, Tag, Dropdown, Menu } from "antd";

import Input from "../components/InputSearch";
import { FilterOutlined } from "@ant-design/icons";

const menu = (
  <Menu>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="http://www.tmall.com/">
        xem chi tiet
      </a>
    </Menu.Item>
  </Menu>
);

function Index() {
  const token = localStorage.getItem("token");
  const [Orderdoc, setOrderdoc] = useState([]);

  const getOrderDoc = () => {
    axios({
      baseURL: "https://besins-doctor-dev.wejelly.com/api/",
      method: "get",
      url:
        "order/listForDoctor?&page=1&statusSelect=SUBMIT,PROCESSING,PROCEED,DELIVERING,COMPLETED,BACK_ORDER,RETURN,CANCELED&status=[%22SUBMIT%22,%22PROCESSING%22,%22PROCEED%22,%22DELIVERING%22,%22COMPLETED%22,%22BACK_ORDER%22,%22RETURN%22,%22CANCELED%22]",
      headers: {
        "api-token": token,
      },
    })
      .then((res) => {
        console.log(res);
        setOrderdoc(res.data.data.docs);
      })
      .catch((error) => {
        //api loi handle here!
      });
  };
  useEffect(() => {
    getOrderDoc();
  }, []);

  return (
    <Appp>
      <Ordermanagement>
        <h2>Quản lý đơn hàng </h2>
        <OrderSearch>
          <Input />
          <FilterOutlined />
        </OrderSearch>
        {Orderdoc.map((item, key) => {
          return (
            <StyleCard key={key}>
              <div className="header-card">
                <span>{item.orderId}</span>
                <Dropdown overlay={menu} placement="bottomRight">
                  <Button>...</Button>
                </Dropdown>
              </div>
              <Tag className="tag" color="orange">
                {item.status}
              </Tag>
              <div className="applicationcreator">
                <span>Người tạo đơn : </span>
                <span>{item.buyer.fullName}</span>
              </div>
              <div className="time">{item.createdAt}</div>
            </StyleCard>
          );
        })}

        <StylePagination defaultCurrent={1} pageSize={1} />
      </Ordermanagement>
    </Appp>
  );
}

export default Index;
