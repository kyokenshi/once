import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Khachhang from "./Khachhang";
import Events from "./eventPage";
import Management from "./Ordermanagement";
import Login from "./Login/Login";
import Promotions from "./Promotion/promotion";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/" component={Khachhang} />
          <Route exact path="/login" component={Login} />

          <Route path="/events" component={Events} />
          <Route path="/management" component={Management} />
          <Route path="/promotion" component={Promotions} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
