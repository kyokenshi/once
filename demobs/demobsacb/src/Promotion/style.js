import styled from "styled-components";
import "antd/dist/antd.css";

export const PromotionPage = styled.div`
    padding : 16px ;
    hr{
         margin : 16px 0px ;
    }
`
export const PromotionItem = styled.div `
    padding-left : 28px ;
    display : flex ;
    justify-content :space-between ;
    align-items : center ; 
    .itempromotion{
      width: calc(100% - 100px);
    }

`