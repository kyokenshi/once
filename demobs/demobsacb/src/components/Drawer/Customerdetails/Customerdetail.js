import React, { useEffect, useState } from "react";
import { Menu, Avatar } from "antd";

import { Header, StyleWrapDrawer } from "../../Drawer/style";
import back from "../../../assets/images/back.png";
import {
  SectionMenu,
  SectionMenuSetting,
  StyleDropdown,
  Myorder,
} from "../Customerdetails/style";
import orderlist from "../../../assets/images/orderlist.png";
import event from "../../../assets/images/event24px.png";
import vietnam from "../../../assets/images/vietnam24px.png";
import group from "../../../assets/images/group24px.png";

/////
import DrawerCalendar from "../Calendar/Calendar";

function Customerdetail(props) {
  function renderTitle() {
    return (
      <Header>
        <div className="title">
          <span>Thông báo</span>
        </div>
        <img src={back} className="imgback" onClick={props.onClose}></img>
      </Header>
    );
  }

  const [eventdrw, setEventdrw] = useState(false);

  const handleClickevent = () => {
    setEventdrw(true);
  };
  const [info, setInfo] = useState({});
  useEffect(() => {
    const userInfo = JSON.parse(localStorage.getItem("userInfo"));
    setInfo(userInfo);
    console.log(userInfo);
  }, []);

  const menu = (
    <Menu>
      <Menu.Item>
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="http://www.alipay.com/"
        >
          Đăng xuất
        </a>
      </Menu.Item>
    </Menu>
  );

  return (
    <StyleWrapDrawer
      title={renderTitle()}
      placement="right"
      closable={false}
      visible={props.open}
      width="100%"
    >
      <SectionMenu>
        <SectionMenuSetting>
          <StyleDropdown
            overlay={menu}
            placement="topLeft"
            arrow
            trigger={["click"]}
          >
            <div className="dropdown-text">
              <Avatar size={32} src="" />
              <div className="text">
                <span>
                  <h3>Bác sĩ</h3>
                  <span>{info.fullName}</span>
                </span>
              </div>
            </div>
          </StyleDropdown>
          <Myorder>
            <img src={orderlist}></img>
            <span> Đơn hàng của bạn</span>
          </Myorder>
          <Myorder>
            <img src={group}></img>
            <span> Thông tin khuyến mãi</span>
          </Myorder>
          <Myorder onClick={handleClickevent}>
            <img src={event}></img>
            <span> hội thảo của tôi</span>
          </Myorder>

          <Myorder>
            <img src={vietnam}></img>
            <span> Tiếng Việt</span>
          </Myorder>
        </SectionMenuSetting>
      </SectionMenu>

      <DrawerCalendar eventdrw={eventdrw} onClose={() => setEventdrw(false)} />
    </StyleWrapDrawer>
  );
}

export default Customerdetail;
