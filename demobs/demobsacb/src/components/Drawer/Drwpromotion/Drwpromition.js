import React from "react";
import { Header, StyleWrapDrawer } from "../../Drawer/style";
import back from "../../../assets/images/back.png";
import { Section } from "./style";
import moment from "moment";

function Drwpromition(props) {
  const { promotions } = props;
  // console.log(promotions);
  function renderTitle() {
    return (
      <Header>
        <div className="title">
          <span>Thông báo</span>
        </div>
        <img src={back} className="imgback" onClick={props.onClose}></img>
      </Header>
    );
  }

  return (
    <StyleWrapDrawer
      title={renderTitle()}
      // headerStyle={{ color: "red" }}
      // placement={placement}
      // closable={false}
      closable={false}
      // onClose={props.onClose}
      visible={props.visible}
      width="100%"
      // key={placement}
    >
      <Section>
        <div className="promotion-detail">
          <div className="promotion-content">
            <div className="title">Tên khuyến mãi</div>

            <div className="description">{promotions?.title}</div>
          </div>
          <div className="promotion-content">
            <div className="title">Sản phẩm</div>
            <div className="description">{promotions?.product?.name}</div>
          </div>
          <div className="promotion-content">
            <div className="title">Khuyến mãi</div>
            <div className="description">% {promotions?.percent}</div>
          </div>
          <div className="promotion-content">
            <div className="title">hiệu lực đến</div>
            <div className="description">
              {moment(promotions?.timeEnd).format("DD/MM/YYYY")}
            </div>
          </div>
          <div className="promotion-content">
            <div className="title">
              Điều kiến khuyến mãi <span>(Trên số lần mua)</span>
            </div>
            <div className="description">
              Số lượng tối thiểu phải mua: <span>{promotions?.amount}</span> hộp
            </div>
          </div>
        </div>
      </Section>
    </StyleWrapDrawer>
  );
}

export default Drwpromition;
