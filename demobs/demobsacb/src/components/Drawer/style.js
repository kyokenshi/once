import styled from "styled-components";
import "antd/dist/antd.css";
import { Drawer } from "antd";
import { Tabs } from "antd";

//   const { TabPane } = Tabs;

// sửa button hoặc drawer ant = ghi de styled
export const StyleWrapDrawer = styled(Drawer)`
  .ant-drawer-header,
  .ant-drawer-body {
    padding: 0;
  }
`;

export const Header = styled.div`
  position: relative;
  background: #f5f6f7;
  height: 56px;
  .title {
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    font-style: normal;
    font-weight: 600;
    font-size: 20px;
    color: #172b4d;
  }
  img {
    position: absolute;
    top: 35%;
    left: 5%;
  }
`;

export const StyleTabs = styled(Tabs)`
  .ant-tabs-nav-list {
    width: 100%;
    .ant-tabs-tab {
      width: 50%;
      margin-right: 16px;
      display: flex;
      justify-content: center;
      color: #42526e;
      font-weight: 600;
    }
  }
  .read-mark {
    padding: 8px;
    text-align: right;
    font-size: 12px;
    line-height: 16px;
    color: #0073c4;
    font-weight: 200px;
    font-family: "Courier New", Courier, monospace;
  }
  .content {
    padding: 16px;
    border-top: 1px solid #e6e8eb;
    background-color : #f5f6f7 ;
    .content-header {
      .text1 {
        margin-right: 8px;
        font-weight: 600;
        font-size: 12px;
        line-height: 16px;
        color: #172b4d;
      }
    }
    .content-detail {
      padding-top: 16px;
      padding-bottom: 8px;
      margin-bottom: 0;
    }
    .content-time {
      margin-bottom: 0;
      color: #6b778c;
    }
  }
          
`;

export const SectionMenu = styled.div`

  position : relative ;

`
