import { Button } from 'antd';
import styled from "styled-components";
import "antd/dist/antd.css";

export const StyledButton = styled(Button)`
 
       margin : 20px;
      color: #ffffff;
    border-color: #0073C4;
    background-color: #0073C4;
    text-shadow: none;
    border-radius: 4px;
    font-weight: 600; 
    
   

`;
