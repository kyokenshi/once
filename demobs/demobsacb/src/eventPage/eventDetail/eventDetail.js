import React from "react";
import { Header, StyleWrapDrawer } from "../../components/Drawer/style";
import {
  EventDetail,
  Detailcontent,
  DetailcontentCollapse,
} from "../eventDetail/style";
import back from "../../assets/images/back.png";
import Cardimg from "../../assets/images/card1.png";
import calendar from "../../assets/images/calendar.png";
import { Tag } from "antd";
import { Collapse } from "antd";

function eventDetail(props) {
  const { eventdetail } = props;
  function renderTitle() {
    return (
      <Header>
        <div className="title">
          <span>Chi tiết hội thảo</span>
        </div>
        <img src={back} className="imgback" onClick={props.onClose}></img>
      </Header>
    );
  }

  const { Panel } = Collapse;

  return (
    <StyleWrapDrawer
      title={renderTitle()}
      closable={false}
      onClose={props.onClose}
      visible={props.visible}
      width="100%"
      // key={placement}
    >
      <EventDetail>
        <img src={Cardimg} className="imgcard"></img>
        <Detailcontent>
          <div className="event-detail-calendar mb-1">
            <img src={calendar} className="mr-1"></img>
            <span>{eventdetail.timeTakesPlace}</span>
          </div>
          <div className="event-detail-title mb-1">
            {eventdetail.description}
          </div>
          <Tag
            color={eventdetail.type === "OFFLINE" ? "processing" : "success"}
          >
            {eventdetail.type === "OFFLINE"
              ? "Hội thảo gặp gỡ trực tiếp"
              : "Hội thảo trực tuyến"}
          </Tag>
          <div className="event-detail-title2 mt-2">
            <hr></hr>
            <span>
              Chương trình đào tạo liên tục "Cập nhật mới trong điều trị các
              dạng viêm bàng quang mạn tính" được tổ chức Hội Tiết niệu - Thận
              học TP. HCM (HUNA) với sự tài trợ của Besins Healthcare Thời gian:
              9h00 - 11h00, Thứ Bảy, ngày 21/11/2020
            </span>
          </div>
        </Detailcontent>
        <Collapse defaultActiveKey={["1"]} ghost expandIconPosition="right">
          <Panel header="Báo cáo viên" key="1">
            <div className="">
              <span className="ml-4">Chủ tọa : </span>
              <ul>
                <li className className="ml-4">
                  <span>PGS. TS. Nguyễn Tuấn Vinh</span>
                  <span>Chủ tịch Hội Tiết niệu - Thận học TP. HCM</span>
                </li>
              </ul>
              <span className="ml-4">Báo giáo viên </span>
              <ul>
                <li className className="ml-4">
                  <span>PGS. TS. Nguyễn Tuấn Vinh</span>
                  <span>Chủ tịch Hội Tiết niệu - Thận học TP. HCM</span>
                </li>
              </ul>
            </div>
          </Panel>
          <Panel header="Người liên hệ" key="2">
            <div>
              <ul className="">
                <li>
                  <strong>Họ và tên : </strong>
                  <span>MS Lê Hoàng Anh</span>
                </li>
                <li>
                  <strong>Số điện thoại : </strong>
                  <span>0976024242</span>
                </li>
                <li>
                  <strong>Email : </strong>
                  <span>abc123@gmail.com</span>
                </li>
              </ul>
            </div>
          </Panel>
          <Panel header="hướng dẫn tham dự" key="3">
            <p>Hướng dẫn tham dự</p>
          </Panel>
          <Panel header="Thông báo về hội thảo" key="3">
            <p>báo cáo giáo viên</p>
          </Panel>
        </Collapse>
      </EventDetail>
    </StyleWrapDrawer>
  );
}

export default eventDetail;
