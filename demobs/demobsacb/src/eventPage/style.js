
import styled from "styled-components";
import "antd/dist/antd.css";


export const HomeEvent = styled.div`
   color: #172b4d;
    font-size: 20px;
    font-weight: 700;
    text-align: center;
    padding: 14px 0;

`;

export const StyleHeader = styled.div`
    width : 100% ;

`;

export const StyleSection = styled.div`

    .text30day{
        color: #172b4d;
    font-size: 18px;
    font-weight: 600;
    line-height: 24px;
    margin-bottom: 20px;
    padding : 0 16px;
    }

`;
